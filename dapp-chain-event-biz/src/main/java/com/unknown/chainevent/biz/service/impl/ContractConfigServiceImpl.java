package com.unknown.chainevent.biz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.unknown.chainevent.biz.event.InitLogFilter;
import com.unknown.chainevent.biz.service.ChainConfigService;
import com.unknown.chainevent.biz.service.ContractConfigService;
import com.unknown.chainevent.common.entity.ChainConfig;
import com.unknown.chainevent.common.entity.ContractConfig;
import com.unknown.chainevent.common.mapper.ContractConfigMapper;
import org.cloud.exception.BusinessException;
import org.cloud.vo.CommonApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class ContractConfigServiceImpl extends ServiceImpl<ContractConfigMapper, ContractConfig> implements ContractConfigService{

    @Autowired
    private InitLogFilter initLogFilter;
    @Autowired
    private ChainConfigService chainConfigService;
    @Override
    public int insertSelective(ContractConfig record) {
        return baseMapper.insertSelective(record);
    }
    @Override
    public int updateByPrimaryKeySelective(ContractConfig record) {
        return baseMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CommonApiResult<?> addContract(ContractConfig config) throws BusinessException {
//        LoginUserDetails loginUser = FeignUtil.single().getLoginUser();
//        config.setCreateBy(loginUser.getUsername());
//        config.setUpdateBy(loginUser.getUsername());
        ChainConfig chainConfig = chainConfigService.getById(config.getChainConfigId());
        config.setChainSymbol(chainConfig.getChainSymbol());
        int insert = getBaseMapper().insertSelective(config);
        if (insert == 0) {
            throw new BusinessException("修改失败");
        }
        initLogFilter.clearLogFilter(chainConfig.getChainConfigId());
        return CommonApiResult.createSuccessResult();
    }

    @Override
    public CommonApiResult<?> updateContract(ContractConfig config) throws BusinessException {
        ContractConfig contractConfig = baseMapper.selectById(config.getContractConfigId());
        if (ObjectUtils.isEmpty(contractConfig)) {
            throw new BusinessException("合约配置不存在");
        }
        ContractConfig updateContractConfig = ContractConfig.builder()
                .contractConfigId(config.getContractConfigId())
                .contractType(config.getContractType())
                .name(config.getName())
                .symbol(config.getSymbol())
                .contractStatus(contractConfig.getContractStatus())
                .expand1(config.getExpand1())
                .expand2(config.getExpand2())
                .expand3(config.getExpand3())
                .build();
        boolean isUpdateChain = config.getChainConfigId() != null && !config.getChainConfigId().equals(contractConfig.getChainConfigId());
        ChainConfig chainConfig = chainConfigService.getById(contractConfig.getChainConfigId());
        if (isUpdateChain){
            updateContractConfig.setChainConfigId(chainConfig.getChainConfigId());
            updateContractConfig.setChainSymbol(chainConfig.getChainSymbol());
        }
//        config.setUpdateBy(FeignUtil.single().getLoginUser().getUsername());
        int insert = baseMapper.updateByPrimaryKeySelective(config);
        if (insert == 0) {
            throw new BusinessException("修改失败");
        }
        initLogFilter.clearLogFilter(chainConfig.getChainConfigId());
        return CommonApiResult.createSuccessResult();
    }

    @Override
    public CommonApiResult<?> updateStatus(Long contractConfigId, Integer status) throws BusinessException {
        ContractConfig contractConfig = baseMapper.selectById(contractConfigId);
        if (ObjectUtils.isEmpty(contractConfig)) {
            throw new BusinessException("链配置不存在");
        }
        ContractConfig config = new ContractConfig();
        config.setContractConfigId(contractConfigId);
        config.setContractStatus(status);
        int update = baseMapper.updateByPrimaryKeySelective(config);
        if (update == 0) {
            throw new BusinessException("更新失败");
        }
        ChainConfig chainConfig = chainConfigService.getById(contractConfig.getChainConfigId());
        initLogFilter.clearLogFilter(chainConfig.getChainConfigId());
        return CommonApiResult.createSuccessResult();
    }

    @Override
    public ContractConfig getBySymbol(String symbol) {
        QueryWrapper<ContractConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(ContractConfig::getSymbol, symbol);
        return getBaseMapper().selectOne(queryWrapper);
    }
}
