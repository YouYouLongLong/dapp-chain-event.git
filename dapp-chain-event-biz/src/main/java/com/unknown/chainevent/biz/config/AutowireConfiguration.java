package com.unknown.chainevent.biz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.unknown.chainevent.biz.*")
public class AutowireConfiguration {
}
