/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.68.141
 Source Server Type    : MySQL
 Source Server Version : 80100 (8.1.0)
 Source Host           : 192.168.68.141:10002
 Source Schema         : kiser_dapp-admin

 Target Server Type    : MySQL
 Target Server Version : 80100 (8.1.0)
 File Encoding         : 65001

 Date: 23/11/2024 19:57:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_chain_config
-- ----------------------------
DROP TABLE IF EXISTS `t_chain_config`;
CREATE TABLE `t_chain_config`  (
  `chain_config_id` bigint NOT NULL AUTO_INCREMENT,
  `rpc_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'rpc地址',
  `chain_symbol` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部署链,部署在那条链上,如ETH,bsc测试链等',
  `block_number` bigint NOT NULL COMMENT '区块高度，初始化为部署的区块高度。',
  `chain_id` bigint NULL DEFAULT NULL COMMENT '链的chain id',
  `chain_status` int NOT NULL COMMENT '链状态(10启用 20禁用)',
  `confirm_block` int NOT NULL DEFAULT 3 COMMENT '确认区块数',
  `distrust_block` int NOT NULL DEFAULT 1 COMMENT '不信任区块数（扫块区块为：最新快-distrust_block）',
  `scan_old_block` int NOT NULL DEFAULT 0 COMMENT '重复扫描旧的区块数',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`chain_config_id`) USING BTREE,
  UNIQUE INDEX `chain_symbol_udx`(`chain_symbol` ASC) USING BTREE,
  UNIQUE INDEX `chainidl_udx`(`chain_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'NFT链配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_contract_config
-- ----------------------------
DROP TABLE IF EXISTS `t_contract_config`;
CREATE TABLE `t_contract_config`  (
  `contract_config_id` bigint NOT NULL AUTO_INCREMENT,
  `chain_config_id` bigint NOT NULL COMMENT '关联链ID',
  `chain_symbol` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '链符号',
  `contract_address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '合约地址',
  `contract_type` int NOT NULL COMMENT '合约类型：10：ERC20合约 20：REC721合约 30：其他合约',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `symbol` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '符号',
  `contract_status` int NOT NULL COMMENT '合约配置状态(10启用 20禁用)',
  `expand1` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '拓展字段1',
  `expand2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '拓展字段2',
  `expand3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '拓展字段3',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`contract_config_id`) USING BTREE,
  UNIQUE INDEX `udx_chainconfigid_contractaddress`(`chain_config_id` ASC, `contract_address` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'NFT合约配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_contract_logs
-- ----------------------------
DROP TABLE IF EXISTS `t_contract_logs`;
CREATE TABLE `t_contract_logs`  (
  `contract_log_id` bigint NOT NULL AUTO_INCREMENT,
  `chain_config_id` bigint NOT NULL,
  `chain_symbol` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '链符号',
  `contract_config_id` bigint NOT NULL COMMENT '合约配合id',
  `contract_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '事件所在合约地址',
  `trans_hash` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '日志所在交易hash',
  `index_for_block` bigint NOT NULL COMMENT '日志序号（相对区块）',
  `trans_from` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '原始交易发起方',
  `trans_to` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '原始交易调用的合约',
  `type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型，暂时不知道何用',
  `first_topic` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'topic0，代表日志名字',
  `second_topic` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'topic1',
  `third_topic` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'topic2',
  `fourth_topic` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'topic3',
  `log_data` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日志的常规数据(16进制)',
  `confirm_status` int NOT NULL DEFAULT 10 COMMENT '确认状态(10-未确认；20-有效；21-无效)',
  `status` int NOT NULL COMMENT '处理状态(10未处理 20已处理)',
  `block_number` bigint NOT NULL COMMENT '区块高度',
  `block_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '所在区块时间戳',
  `method_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci GENERATED ALWAYS AS (substr(`first_topic`,1,10)) STORED NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`contract_log_id`) USING BTREE,
  UNIQUE INDEX `transhash_and_idx_inx`(`trans_hash` ASC, `index_for_block` ASC) USING BTREE,
  INDEX `firsttopic_status_inx`(`first_topic` ASC, `status` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 933 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '转账记录日志表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
