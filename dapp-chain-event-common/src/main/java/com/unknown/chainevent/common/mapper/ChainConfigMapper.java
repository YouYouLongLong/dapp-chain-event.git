package com.unknown.chainevent.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unknown.chainevent.common.entity.ChainConfig;
import org.apache.ibatis.annotations.Param;

public interface ChainConfigMapper extends BaseMapper<ChainConfig> {
    int insertSelective(ChainConfig record);

    int updateByPrimaryKeySelective(ChainConfig record);

    /**
     * 更新最后区块
     *
     * @param id
     * @param block
     * @return
     */
    int updateLastBlock(@Param("id") Long id, @Param("block") Long block);
}