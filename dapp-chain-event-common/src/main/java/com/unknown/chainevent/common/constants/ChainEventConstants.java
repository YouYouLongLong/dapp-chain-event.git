package com.unknown.chainevent.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface ChainEventConstants {

    /**
     * 合约日志处理状态
     */
    @Getter
    @AllArgsConstructor
    public static enum ContractLogStatus {

        UNTREATED(10, "未处理"),
        PROCESSED(20, "已处理"),
        ;

        private int status;
        private String desc;
    }

    /**
     * 合约日志交易确认状态
     */
    @Getter
    @AllArgsConstructor
    enum ContractLogConfirmStatus {

        AWAIT(10, "未确认"),
        SUCCESS(20, "有效"),
        FAIL(21, "无效"),
        ;

        private int status;
        private String desc;
    }

    /**
     * 链状态
     */
    @Getter
    @AllArgsConstructor
    public static enum ChainStatusConstants {
        NORMAL(10, "启用"),
        FORBIDDEN(20,"禁用"),
        ;
        private int status;
        private String desc;
    }

    /**
     * 支持的链符号
     */
    @Getter
    @AllArgsConstructor
    public static enum ChainSymbolEnum {

        BSC("bsc"),
        ;

        private String symbol;
    }

    /**
     * 合约配置状态
     */
    @Getter
    @AllArgsConstructor
    public static enum ContractStatusConstants {
        NORMAL(10, "启用"),
        FORBIDDEN(20,"禁用"),
        ;
        private int status;
        private String desc;
    }

    /**
     * 合约类型枚举
     */
    @Getter
    @AllArgsConstructor
    public static enum ContractTypeConstants {
        ERC20(10, "ERC20标准合约"),
        ERC721(20,"ERC721标准合约"),
        OTHERS(30,"其它合约"),
        ;
        private int type;
        private String desc;
    }
}
