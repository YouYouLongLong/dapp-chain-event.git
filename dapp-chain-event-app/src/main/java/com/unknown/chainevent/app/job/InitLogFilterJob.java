package com.unknown.chainevent.app.job;

import com.unknown.chainevent.biz.event.InitLogFilter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.cloud.scheduler.constants.MisfireEnum;
import org.cloud.scheduler.job.BaseQuartzJobBean;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 初始化交易订阅任务
 */
@Component
@Slf4j
public class InitLogFilterJob extends BaseQuartzJobBean {

    @Autowired
    private InitLogFilter initLogFilter;

    @Override
    protected void init() {
        this.jobName = "初始化事件扫描任务";
        jobData.put("description", "初始化事件扫描任务");
        this.jobTime = "0/30 * * * * ?";
        setMisfire(MisfireEnum.CronScheduleMisfireEnum.MISFIRE_INSTRUCTION_DO_NOTHING);
    }

    @SneakyThrows
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        initLogFilter.initSubscribe();
    }
}