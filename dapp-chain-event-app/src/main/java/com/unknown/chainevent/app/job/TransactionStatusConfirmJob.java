package com.unknown.chainevent.app.job;

import com.unknown.chainevent.biz.service.ContractLogsService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.cloud.scheduler.constants.MisfireEnum;
import org.cloud.scheduler.job.BaseQuartzJobBean;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 交易状态确认任务
 */
@Component
@Slf4j
public class TransactionStatusConfirmJob extends BaseQuartzJobBean {

    @Autowired
    private ContractLogsService contractLogsService;

    @Override
    protected void init() {
        this.jobName = "事件交易状态确认任务";
        jobData.put("description", "事件交易状态确认任务");
        this.jobTime = "0/5 * * * * ?";
        setMisfire(MisfireEnum.CronScheduleMisfireEnum.MISFIRE_INSTRUCTION_DO_NOTHING);
    }

    @SneakyThrows
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        contractLogsService.processTransactionConfirm();
    }
}