package com.unknown.chainevent.app.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.cloud.mybatisplus.annotation.Query;

import java.util.List;

@ApiModel("合约事件查询参数")
@Data
public class ContractLogsQueryParamDTO {
    @ApiModelProperty(value = "")
    private Long contractLogId;

    @ApiModelProperty(value = "")
    private Long chainConfigId;


    @ApiModelProperty(value = "合约配合id")
    private Long contractConfigId;


    @ApiModelProperty(value = "日志所在交易hash")
    private String transHash;


    @ApiModelProperty(value = "原始交易发起方")
    private String transFrom;


    @ApiModelProperty(value = "topic0，代表日志名字")
    @Query(type = Query.Type.LEFT_LIKE)
    private String firstTopic;

    @ApiModelProperty(value = "确认状态(10-未确认；20-有效；21-无效)")
    private Integer confirmStatus;

    @ApiModelProperty(value = "处理状态(10未处理 20已处理)")
    private Integer status;

    @ApiModelProperty(value = "区块高度")
    private Long blockNumber;


    @ApiModelProperty(value = "区块时间范围，格式yyyy-MM-dd HH:mm:ss")
    @Query(type = Query.Type.UNIX_TIMESTAMP)
    private List<String> blockTimestamp;
}
